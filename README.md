# Programação Orientada a Objetos - Ciência da Computação - UERN \[[English version]\](#english-version)

Exemplos de código fonte Java e slides da disciplina Programação Orientada a Objetos do Curso de Ciência da Computação da UERN-Mossoró.

São bem vindas contribuições que:
* Adicionem comentários aos programas;
* Traduzam variáveis e comentários para Inglês / Português;
* Resolvam os problemas propostos com outras abordagens.

#english-version
# Object Oriented Programming - Computer Science - UERN

Slides and Java example source code for Object Oriented Programming discipline in Computer Science course at UERN-Mossoró-Brazil.

We welcome contributions that:
* Add comments to programs;
* Translate variables and comments to English / Portuguese;
* Solve proposed problems with another approaches.
